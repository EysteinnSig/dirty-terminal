
#include <functional>
#include "../dirty_terminal.h"

int main() {

  using namespace DirtyTerminal;
  Terminal term(AnsiCommands::colorize_fg(230, 50, 200, "Command: "));

  bool exit = false;
  term.on_enter = [&term, &exit](const std::string &cmd) { 
      term.print_text(">"+cmd); 
      if (cmd == "exit") exit=true; 
    };
  term.on_special_key = [&term, &exit](DirtyTerminal::KEY key) {
      term.print_text("Special Key: "+std::to_string((int)key)); 
      if (key == DirtyTerminal::KEY::CTRL_C) 
          exit=true; 
    };

  std::vector<std::string> commands = {"help", "exit", "compare", 
        AnsiCommands::colorize_fg(200, 10, 10, "command1"), AnsiCommands::colorize_fg(10, 200, 10, "command2")};
  term.on_hints = [&term, &commands](const std::string &part) -> std::vector<std::string>
  {
      std::vector<std::string> ret;
      for (auto cmd : commands)
      {
          if (part == term.purge_ansi(cmd).substr(0, part.size()))
            ret.push_back(cmd);
      }
      return ret;
  };
  //t.reset_command("Some dummy command");
  term.print_text("Press ctrl-c or write 'exit' to quit.  Press tab for hints.");
  term.run(exit);
  
  return 0;
}

