# Dirty Terminal

Quick and dirty terminal and command line.  Single header file.

# Usability
I wrote this code to gain an interactive shell to other programs that I'm writing. Now I can log to console as usual but also send commands to the program to execute certain code or change variables for example.

By adding this single header only project, its easy to attach an interactive shell to any c++ project in few lines of code. 

- [X] All code in single header file and only few hundred lines.
- [X] Uses lambdas to connect to important parts (for example when a command is executed)
- [X] Supports TAB triggered hints.
- [X] Supports ANSI color codes.
- [X] Supports UTF8.
- [X] Can run in its own loop or integrated into any main loop with a step function.
- [ ] History, scroll though previous commands
- [ ] Search history (ctrl-r)
- [ ] Thread safe, print to console from different thread


# Examples

A very basic console terminal running in its own loop
```c++
using namespace DirtyTerminal;
Terminal term(AnsiCommands::colorize_fg(230, 50, 200, "Command: "));
bool exit = false;
term.on_enter = [&term, &exit](const std::string &cmd) { 
    term.print_text(">"+cmd); 
    if (cmd == "exit") exit=true; 
};
std::vector<std::string> commands = {"help", "exit", "compare", 
    AnsiCommands::colorize_fg(200, 10, 10, "command1"), AnsiCommands::colorize_fg(10, 200, 10, "command2")};
term.on_hints = [&term, &commands](const std::string &part) -> std::vector<std::string>
{
    std::vector<std::string> ret;
    for (auto cmd : commands)
    {
        if (part == term.purge_ansi(cmd).substr(0, part.size()))
        ret.push_back(cmd);
    }
    return ret;
};
term.print_text("Press ctrl-c or write 'exit' to quit..  Press tab for hints.");
term.run(exit);
```

Resulting in:

![alt text](https://gitlab.com/EysteinnSig/dirty-terminal/raw/main/examples/basic.png)
