cmake_minimum_required(VERSION 3.0.0)
project(dirty-terminal VERSION 0.1.0)

include(CTest)
enable_testing()

add_executable(dirty-terminal main.cpp)

add_executable(basic examples/basic.cpp)


set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
