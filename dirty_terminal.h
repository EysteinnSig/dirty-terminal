#pragma once

#if defined(WIN32) || defined(__WIN32__) || defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
    #define DIRTY_TERM_WINDOWS
#elif defined(unix) || defined(__unix) || defined(__unix__) || defined(__linux__)
    #define DIRTY_TERM_LINUX
#elif defined(__APPLE__) || defined(__MACH__)
    #error Not supported
#elif defined(__ANDROID__)
    #error Not supported
#endif



#include <regex>
#include <string>
#include <unistd.h>
//#include "utf8.h"
#include <sstream>
#include <functional>
#include <iostream>

#if defined(DIRTY_TERM_WINDOWS)
#include <windows.h>
#endif

#if defined(DIRTY_TERM_LINUX)
#include <sys/ioctl.h>
#include <termios.h>
#endif

//struct termios org_termios;
namespace DirtyTerminal {

enum class KEY // : uint8_t 
{
	KEY_NULL = 0,	    /* NULL */
	CTRL_A = 1,         /* Ctrl+a */
	CTRL_B = 2,         /* Ctrl-b */
	CTRL_C = 3,         /* Ctrl-c */
	CTRL_D = 4,         /* Ctrl-d */
	CTRL_E = 5,         /* Ctrl-e */
	CTRL_F = 6,         /* Ctrl-f */
	CTRL_H = 8,         /* Ctrl-h */
	TAB = 9,            /* Tab */
    CTRL_J = 10,        // Line feed
	CTRL_K = 11,        /* Ctrl+k */
	CTRL_L = 12,        /* Ctrl+l */
	ENTER = 13,         /* Enter */
	CTRL_N = 14,        /* Ctrl-n */
	CTRL_P = 16,        /* Ctrl-p */
	CTRL_T = 20,        /* Ctrl-t */
	CTRL_U = 21,        /* Ctrl+u */
	CTRL_W = 23,        /* Ctrl+w */
	ESC = 27,           /* Escape */
    SPACE = 32,         
	BACKSPACE =  127    /* Backspace */
};

namespace AnsiCommands {


    std::string cursor_up(int k) 
    { 
        std::ostringstream stream;
        stream << "\033[" << k << "A";
        return stream.str();
    }
    std::string cursor_down(int k) 
    { 
        std::ostringstream stream;
        stream << "\033[" << k << "B";
        return stream.str();
    }
    std::string cursor_right(int k)
    { 
        std::ostringstream stream;
        stream << "\033[" << k << "C";
        return stream.str();
    }

    inline std::string cursor_left_margin() { return "\r"; }

    // clear screen down from cursor
    inline std::string clear_down() { return "\033[0J"; }


    // Go to left margin, up cursorpos/term_col lines and clear to bottom of screen.
    // Leaves cursor at cursorpos = 0
    std::string erase_commandline(int cursorpos, int term_col)
    {
        //int term_col = get_term_cols();
        int up = cursorpos/term_col;
        // goto start of line
        std::string cmd = cursor_left_margin();
        // go up few rows as needed
        if (up>0) cmd += cursor_up(up);
        // clear screen down from cursor
        cmd += clear_down();
        return cmd;
    }

    // Print out commandline (prompt + command). Assumes cursorpos = 0 and leves cursor at end of line.
    std::string print_commandline(const std::string &commandline, int commandline_print_len, int term_col)
    {
        /*std::string str = ansi_erase_commandline();
        command_txt = new_command;
        command_len = measure_print_length(new_command);
        str = str + (prompt_txt+command_txt);
        cursorpos = prompt_len+command_len;

        int term_col = get_term_cols();*/
        std::string str = commandline;
        if (commandline_print_len - (commandline_print_len/term_col)*term_col == 0)
            str += "\r\n";
        return str;
    }

    std::string move_cursor(int currpos, int newpos, int term_col)
    {
        if (newpos == currpos) return "";

        int up = (currpos)/term_col;
        // goto start of line
        std::string cmd = "\r";
        // go up few rows as needed
        if (up>0) cmd += cursor_up(up);

        int down = (newpos)/term_col;
        if (down>0) cmd += cursor_down(down); //std::string("\033[") + std::to_string(down) + "B";

        int right = newpos - (newpos/term_col)*term_col;
        if (right>0) cmd += cursor_right(right); //std::string("\033[") + std::to_string(right) + "C";
        
        return cmd;
    }

    /* Updates the commandline and attemts to leave the cursor in 'newpos'.
        1) Erase commandline from cursor pos, unaffected by old command line
        2) Print new command line
        3) Moves cursor to new position
    */
    std::string update_commandline(const std::string &commandline, int commandline_len, int term_col, int currpos, int newpos)
    {
        return AnsiCommands::erase_commandline(currpos, term_col) + 
            AnsiCommands::print_commandline(commandline, commandline_len, term_col) +
            AnsiCommands::move_cursor(commandline_len, newpos, term_col);
    }


    /* Colors */
    std::string color_fg(uint8_t r, uint8_t g, uint8_t b) {
        static char buffer[30];
        int len = sprintf(buffer, "\033[38;2;%i;%i;%im", r, g, b);
        return std::string(buffer, len);
    }

    std::string color_bg(uint8_t r, uint8_t g, uint8_t b) {
        static char buffer[30];
        int len = sprintf(buffer, "\033[48;2;%i;%i;%im", r, g, b);
        return std::string(buffer, len);
    }

    inline std::string reset_color() { return "\x1b[0m"; }

    inline std::string colorize_fg(uint8_t r, uint8_t g, uint8_t b, const std::string &text)
    {
        return color_fg(r,g,b) + text + reset_color();
    }
    inline std::string colorize_bg(uint8_t r, uint8_t g, uint8_t b, const std::string &text)
    {
        return color_bg(r,g,b) + text + reset_color();
    }
    inline std::string colorize(uint8_t fg_r, uint8_t fg_g, uint8_t fg_b, uint8_t bg_r, uint8_t bg_g, uint8_t bg_b, const std::string &text)
    {
        return color_bg(fg_r,fg_g,fg_b)+color_bg(bg_r,bg_g,bg_b) + text + reset_color();
    }
};


class Terminal
{
  private:

    int cursorpos = 0;
    std::string prompt_txt = "";
    std::string command_txt = "";
    int prompt_len = 0;
    int command_len = 0;
    std::regex ansi_regex;
    std::string cached_text = "";


    static void die(const char *s) {
        perror(s);
        exit(1);
    }
#if defined(DIRTY_TERM_WINDOWS)
    static void get_handles(HANDLE &console_in, HANDLE &console_out, DWORD &oldMode)
    {
        static HANDLE _console_in=0, _console_out=0;
        static DWORD _oldMode=0;
        console_in = _console_in;
        console_out = _console_out;
        oldMode = _oldMode;
    }
#elif defined(DIRTY_TERM_LINUX)
    static struct termios& get_termios()
    {
        static struct termios org_termios;
        return org_termios;
    }
#endif

    static void disableRawMode() {
#if defined(DIRTY_TERM_WINDOWS)
        HANDLE console_in, console_out;
        DWORD oldMode;
        get_handles(console_in, console_out, oldMode);
        SetConsoleMode(console_in, oldMode);
        console_in = 0;
        console_out = 0;
#endif
#if defined(DIRTY_TERM_LINUX)
        //if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &org_termios) == -1)
        if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &get_termios()) == -1)
            die("tcsetattr");
#endif
    }

    void enableRawMode() {
#if defined(DIRTY_TERM_WINDOWS)
        HANDLE console_in, console_out;
        DWORD oldMode;
        get_handles(console_in, console_out, oldMode);
        if (!console_in) {
            console_in = GetStdHandle(STD_INPUT_HANDLE);
            console_out = GetStdHandle(STD_OUTPUT_HANDLE);

            GetConsoleMode(console_in, &oldMode);
            SetConsoleMode(console_in, oldMode &
                                        ~(ENABLE_LINE_INPUT | ENABLE_ECHO_INPUT |
                                            ENABLE_PROCESSED_INPUT));
        }
#elif defined(DIRTY_TERM_LINUX)        
        if (tcgetattr(STDIN_FILENO, &get_termios()) == -1) die("tcgetattr");
        atexit(disableRawMode);
        struct termios raw = get_termios();
        raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
        raw.c_oflag &= ~(OPOST);
        raw.c_cflag |= (CS8);
        raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
        raw.c_cc[VMIN] = 0;
        raw.c_cc[VTIME] = 1;
        if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("tcsetattr");
#endif
    }

    int utf8_bytes(char c)
    {
        // 0xxxxxxx
        //if (c >> 7 == 0)
        if ((c &  0b10000000) == 0x00)
            return 1;
        
        // 110xxxxx 
        //if ((c >> 5) == 0x06) //6)
        if ((c & 0b11100000) == 0xC0)
            return 2;

        // 1110xxxx
        //if (c >> 4 == 0x0E) //14)
        if ((c & 0b11110000) == 0xE0)
            return 3;

        // 11110xxx
        //if ((c >> 3) == 0x1E)  
        if ((c & 0b11111000) == 0xF0)
            return 4;

        return 0;

    }



    int utf8_measure(const std::string &text)
    {
        int len = 0, ret = 0;
        for (auto iter = text.begin(); iter != text.end(); iter++)
        {
            int bytes = utf8_bytes(*iter);
            if (bytes > 1) iter = iter + bytes - 1;
            len++;
        }    
        return len;
    }

    int get_term_cols()
    {
#if defined(DIRTY_TERM_LINUX)
        struct winsize w;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
        return w.ws_col;
#elif defined(DIRTY_TERM_WINDOWS)
        CONSOLE_SCREEN_BUFFER_INFO csbi;

        GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
        return csbi.srWindow.Right - csbi.srWindow.Left + 1;
#endif
    }
public:
    // Removes ansi decorators from text
    std::string purge_ansi(const std::string &text)
    {
        std::string tmp = std::regex_replace(text, ansi_regex, "");
        return tmp;
    }

    int measure_print_length(const std::string &text)
    {
        return utf8_measure(purge_ansi(text));
    }
private:
    void ansi_write(const std::string &s)
    {
        write(STDOUT_FILENO, s.c_str(), s.size());
    }

    void clear_screen()
    {
        // Move to 1,1 and clear screen to end
        ansi_write("\033[1;1H\033[0J");
    }


    // Map from regular string pos to ansi string pos
    int map_ansi_pos(const std::string &ansitext, int pos)
    {
        //std::regex r("(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]");
        int accu = 0;
        for(std::sregex_iterator i = std::sregex_iterator(ansitext.begin(), ansitext.end(), ansi_regex);
                                i != std::sregex_iterator();
                                ++i )
        {
            std::smatch m = *i;
            if (pos < m.position()-accu)
                break;
            accu = accu + m.str().size();
        }
        return pos+accu;
    }
    int map_utf8_pos(const std::string &utf8_ansi_text, int pos)
    {
        // Map from print pos to account for invisible ansi codes. 
        int ansi_pos = map_ansi_pos(utf8_ansi_text, pos);

        // Account for multi byte characters
        std::string purged = purge_ansi(utf8_ansi_text);
        int utf8_pos = 0;
        for (int i=0; i<pos; i++)
        {
            char c = purged[utf8_pos];
            
            int bytecnt = utf8_bytes(c);
            utf8_pos = utf8_pos + std::max(bytecnt, 1);
        }
        return ansi_pos + (utf8_pos - pos);
    }

    /* Insert text at print position. Fix for utf8 char length and ansi escapes. 
    pos: this is 'printed' character position 
    */
    void ansi_insert(std::string &ansi_text, const std::string ins_str, int pos)
    {
        
        int utf8_ansi_pos = map_utf8_pos(ansi_text, pos);

        // Insert text to the right place and adding difference due to multi byte characters.
        //ansi_text.insert(ansi_pos + (utf8_pos - pos), ins_str);
        ansi_text.insert(utf8_ansi_pos, ins_str);
    }

    void read_symbol(char c)
    {
        int len = utf8_bytes(c);
        char buff[4];
        buff[0] = c;
        for (int k=1; k< len; k++)
        {
            if (read(STDIN_FILENO, &buff[k], 1) == -1) die("read");
        }
        std::string s(buff, len);
        ansi_insert(command_txt, std::string(buff, len), cursorpos-prompt_len);
        
        std::string commandline = prompt_txt + command_txt;
        command_len =  command_len +1; //measure_print_length(command_txt);

        int commandline_len = prompt_len + command_len; //measure_print_length(commandline);
        int term_col = get_term_cols();
        
        ansi_write(
            AnsiCommands::update_commandline(commandline, commandline_len, term_col, cursorpos, cursorpos+1)
        );
        cursorpos++;

    }

    bool read_CSI(char c)
    {
        if (c != 0x1B)
            return false;
        int ret = read(STDIN_FILENO, &c, 1);
        //if (read(STDIN_FILENO, &c, 1) == -1 && errno != EAGAIN) die("read");
        bool ctrl = false;
        bool alt = false;
        bool shift = false;
        if (c == 91)
        {
            read(STDIN_FILENO, &c, 1);
            
            // Modifier
            if (c == '1')
            {
                read(STDIN_FILENO, &c, 1);
                if (c != ';') return false;

                read(STDIN_FILENO, &c, 1);
                if (c-49 == 1) shift = true;
                if (c-49 == 2) alt = true;
                if (c-49 == 4) ctrl = true;

                read(STDIN_FILENO, &c, 1);
            }
            // right
            if (c == 'C') 
            {   
                if (ctrl)
                {
                    int start = cursorpos-prompt_len;
                    std::string purged = purge_ansi(command_txt);
                    start = purged.find_first_not_of(' ', start);
                    if (start == std::string::npos)
                        start = command_len;

                    int newpos = purged.find_first_of(' ', start);
                    if (newpos == std::string::npos)
                    {
                        newpos = prompt_len+command_len;
                    }
                    else
                        newpos = newpos + prompt_len;

                    ansi_write(AnsiCommands::move_cursor(cursorpos, newpos, get_term_cols()));
                    cursorpos = newpos;
                }
                else
                {
                    int newpos = cursorpos+1;
                    newpos = std::min(newpos, prompt_len + command_len);
                    //set_cursor_origin(newpos);
                    ansi_write(AnsiCommands::move_cursor(cursorpos, newpos, get_term_cols()));
                    cursorpos = newpos;
                }
            }

            // left
            if (c == 'D') 
            {
                if (ctrl)
                {
                    int start = cursorpos-prompt_len;
                    if (start == 0) return true;

                    std::string purged = purge_ansi(command_txt);
                    start = purged.find_last_not_of(' ', start-1);
                    if (start == std::string::npos)
                        start = 0;

                    int newpos = purged.find_last_of(' ', start);
                    if (newpos == std::string::npos || newpos == 0)
                    {
                        newpos = prompt_len; //+command_len;
                    }
                    else
                        newpos = newpos + prompt_len +1;

                    ansi_write(AnsiCommands::move_cursor(cursorpos, newpos, get_term_cols()));
                    cursorpos = newpos;
                }
                else {
                    int newpos = cursorpos-1;
                    newpos = std::max(newpos, prompt_len);
                    ansi_write(AnsiCommands::move_cursor(cursorpos, newpos, get_term_cols()));
                    cursorpos = newpos;
                }
            }
            if (c == '3')
            {
                read(STDIN_FILENO, &c, 1);
                if (c == '~') // Delete
                {
                    if (cursorpos < prompt_len+command_len)
                    {
                        int pos = map_utf8_pos(command_txt, cursorpos-prompt_len);
                        command_txt.erase(pos, utf8_bytes(command_txt[pos]));
                        command_len--;

                        std::string commandline = prompt_txt + command_txt;
                        int commandline_len = prompt_len + command_len;
                        int term_col = get_term_cols();
                        ansi_write(
                            AnsiCommands::update_commandline(commandline, commandline_len, term_col, cursorpos, cursorpos)
                        );
                    }
                }
            }
            // Home
            if (c == 'H')
            {
                int term_col = get_term_cols();
                ansi_write(
                    AnsiCommands::move_cursor(cursorpos, prompt_len, term_col)
                );
                cursorpos = prompt_len;
            }
            if (c == 'F')
            {
                int term_col = get_term_cols();
                int commandline_len = prompt_len + command_len;

                ansi_write(
                    AnsiCommands::move_cursor(cursorpos, commandline_len, term_col)
                );
                cursorpos = commandline_len;
            }

        }

        return true;
    }

    void flush_cached_text()
    {
        if (cached_text.empty()) return;
        int term_col = get_term_cols();
        std::string commandline = prompt_txt+command_txt;
        int commandline_len = prompt_len+command_len;
        ansi_write(
            AnsiCommands::erase_commandline(cursorpos, term_col) +
            cached_text +               // cached_text is alwasy followed by \r\n
            AnsiCommands::print_commandline(commandline, commandline_len, term_col) +
            //AnsiCommands::move_cursor(commandline_len, newpos, term_col)
            //prompt_txt + command_txt +    
            AnsiCommands::move_cursor( prompt_len + command_len, cursorpos, term_col)
        );
        cached_text = "";

    }
    
  public:
    
    std::function<void(const std::string &)> on_enter;
    std::function<void(KEY key)> on_special_key;
    std::function<std::vector<std::string> (const std::string &partial)> on_hints;

    const std::string &get_prompt() const { return this->prompt_txt; };
    const std::string &get_command() const { return this->command_txt; };

    void reset_prompt(const std::string &new_prompt)
    {
        int oldlen = prompt_len;
        prompt_len = measure_print_length(new_prompt);
        prompt_txt = new_prompt;
        int newpos = cursorpos - (oldlen-prompt_len);
        ansi_write(
            AnsiCommands::update_commandline(prompt_txt+command_txt, prompt_len+command_len, get_term_cols(), cursorpos, newpos)
        );
        cursorpos = newpos;
    }

    void reset_command(const std::string &new_command)
    {
        int oldlen = command_len;
        command_len = measure_print_length(new_command);
        command_txt = new_command;
        int newpos = std::min(cursorpos, command_len+prompt_len);
        ansi_write(
            AnsiCommands::update_commandline(prompt_txt+command_txt, prompt_len+command_len, get_term_cols(), cursorpos, newpos)
        );
        cursorpos = newpos;
    }
    
    
    void print_text(const std::string &text)
    {
        cached_text = cached_text + text + "\r\n";
    }

    void print_hints(const std::vector<std::string> &hints)
    {
        int maxlen = 0;
        for (auto &str : hints)
        {
            
            //maxlen = std::max(maxlen, (int)str.size());
            maxlen = std::max(maxlen, measure_print_length(str));
        }

        int term_cols = get_term_cols();
        int spaces = 2;
        int hintcols = std::max(1, (int)term_cols / (maxlen + spaces));

        std::string text = "";
        for (int k=0; k<hints.size(); )
        {
            std::string line = "";
            for (int j=0; j<hintcols && k<hints.size(); k++,j++)
            {
                //line = line + hints[k] + std::string(maxlen - hints[k].size() + spaces, ' '); 
                line = line + hints[k] + std::string(maxlen - measure_print_length(hints[k]) + spaces, ' '); 
            }
            print_text(line);
        }

    }

    //https://github.com/yhirose/cpp-linenoise/blob/master/linenoise.hpp
    void step()
    {
        char c = '\0';

        int ret = read(STDIN_FILENO, &c, 1);
        if (ret == -1 && errno != EAGAIN) die("read");
        
        if (!cached_text.empty())
            flush_cached_text();
        //idx++; print_text(std::string("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum: ")+std::to_string(idx));
        if (ret == 0)
            return;
            //continue;
        
        KEY key = (KEY)c;
        switch (key)
        {
        case KEY::ESC: //0x1B:
            read_CSI(c);
            break;

        case KEY::BACKSPACE:
            {
                if (cursorpos<=prompt_len)
                    break;

                // map from print pos to the actual char position in the ansi & utf8 string.
                int pos = map_utf8_pos(command_txt, cursorpos-prompt_len -1);
                command_txt.erase(pos, utf8_bytes(command_txt[pos]));
                command_len--;

                int term_col = get_term_cols();
                std::string commandline = prompt_txt + command_txt;
                int commandline_len = command_len + prompt_len;
                
                ansi_write(
                    AnsiCommands::update_commandline(commandline, commandline_len, term_col, cursorpos, cursorpos-1)
                );
                cursorpos--;
            }
            break;
        case KEY::ENTER:
            {
                //print_text(command_txt);
                if (on_enter)
                    on_enter(command_txt);
                command_len = 0;
                command_txt = "";
                int term_col = get_term_cols();
                ansi_write(
                    AnsiCommands::update_commandline(prompt_txt, prompt_len, term_col, cursorpos, prompt_len)
                );
                cursorpos = prompt_len;
                
            }
            break;
        case KEY::TAB:
            {
            if (!on_hints) break;
            int cmdpos = cursorpos-prompt_len;
            std::string cmd_part = command_txt.substr(0, map_utf8_pos(command_txt, cmdpos));
            int prev_space = cmd_part.find_last_of(' ');
            /*if (prev_space == std::string::npos)
                prev_space = 0;*/
            if (prev_space != std::string::npos)
            //else
            {
                cmd_part = cmd_part.substr(prev_space+1);
            }
            std::vector<std::string> hints = on_hints(cmd_part);
            if (hints.empty()) break;
            
            int term_col = get_term_cols();

            if (hints.size() > 1)
            {
                print_hints(hints);
                break;
            }

            // If there is only one hint, then insert that in command line
            std::string hint = purge_ansi(hints.front());
            std::string missing_part = hint.substr(cmd_part.size());
            ansi_insert(command_txt, missing_part, cmdpos);

            int newcurpos = cursorpos + measure_print_length(missing_part);
            /*std::string::compare()

            int moves = hint.front.size() cmd_part.size()
            //command_txt.erase(command_text.begin()+map_utf8_pos())
            command_txt.insert(cmdpos, hints.front().substr(cmd_part.size()));

            /*std::string cmd = purge_ansi(hints.front());
            int moves = measure_print_length(cmd)-measure_print_length(cmd_part);
            ansi_insert(command_txt, cmd.substr(map_utf8_pos(cmd, moves)), cmdpos);*/
            command_len = command_len + measure_print_length(missing_part); // measure_print_length(command_txt); 
            std::string commandline = prompt_txt + command_txt;
            
            ansi_write(
              AnsiCommands::update_commandline(commandline, prompt_len + command_len, term_col, cursorpos, newcurpos)
            );
            cursorpos = newcurpos;
            }
            break;

        case KEY::CTRL_A:
        case KEY::CTRL_B:
        case KEY::CTRL_C:
        case KEY::CTRL_D:
        case KEY::CTRL_E:
        case KEY::CTRL_F:
        case KEY::CTRL_H:
        case KEY::CTRL_J:
        case KEY::CTRL_K:
        case KEY::CTRL_L:
        case KEY::CTRL_N:
        case KEY::CTRL_P:
        case KEY::CTRL_T:
        case KEY::CTRL_U:
        case KEY::CTRL_W:
            if (on_special_key)
                on_special_key(key);
            break;

        default:
            // Any key higher than space valid character to print to screen
            //if (c >= (int)KEY::SPACE)
                read_symbol(c);
            break;
        }

        if (!cached_text.empty())
            flush_cached_text();
        /*if (read_CSI(c))
            return;
            //continue;
        if (c == 127) // Backspace
        {
            if (cursorpos<=prompt_len)
                return;

            // map from print pos to the actual char position in the ansi & utf8 string.
            int pos = map_utf8_pos(command_txt, cursorpos-prompt_len -1);
            command_txt.erase(pos, utf8_bytes(command_txt[pos]));
            command_len--;

            int term_col = get_term_cols();
            std::string commandline = prompt_txt + command_txt;
            int commandline_len = command_len + prompt_len;
            
            ansi_write(
                AnsiCommands::update_commandline(commandline, commandline_len, term_col, cursorpos, cursorpos-1)
            );
            cursorpos--;
        }
        else if (c == '\r') // Enter - Return
        {
            //print_text(command_txt);
            if (on_enter)
                on_enter(command_txt);
            command_len = 0;
            command_txt = "";
            int term_col = get_term_cols();
            ansi_write(
                AnsiCommands::update_commandline(prompt_txt, prompt_len, term_col, cursorpos, prompt_len)
            );
            cursorpos = prompt_len;
        }
        else if (c == 9)
        {

        }
        else if (c == 'q')
            return;
        else
        {
            read_symbol(c);
        }*/

    }

    //bool should_close = false;
    //void run(bool *exit = nullptr)
    void run(const bool &exit)
    {
        //while (!should_close) {
        while (!exit) {
            
            step();
        }
    }
    void run() { run(false); }

    Terminal(const std::string &prompt) : ansi_regex("(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]")
    {
        enableRawMode();
        clear_screen();
        reset_prompt(prompt);

    }
};



}