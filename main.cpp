
//#include "utf8.h"
#include <functional>
#include "dirty_terminal.h"

/*void test_all()
{
  test_utf8_measure();
}*/

int main() {

  DirtyTerminal::Terminal t("\u001b[31mCommand:\x1b[0m ");

  bool exit = false;
  t.on_enter = [&t, &exit](const std::string &cmd) { t.print_text(">"+cmd);; if (cmd == "exit") exit=true; };
  t.on_special_key = [&t, &exit](DirtyTerminal::KEY key) {
        t.print_text("Special Key2: "+std::to_string((int)key)); 
        if (key == DirtyTerminal::KEY::CTRL_C) exit=true; 
      };
  t.on_hints = [&t](const std::string cmd_part) {
    std::vector<std::string> allhints = {"gregorio", "grotty", "grpck", "grub-fstest", "grub-mkconfig", "grub-mkpasswd-pbkdf2", "grub-probe", \
      "grep", "groupadd", "grpconv", "græbðiþ", "grub-mkdevicemap", "grub-mkrelpath", "grub-reboot", \
      "asldkfj", DirtyTerminal::AnsiCommands::colorize_fg(129, 50, 200, "gresource"), "groupdel", "grpunconv", "grub-mkrescue", "grub-render-label", \
      "groff", "groupmems", "grub-kbdcomp", "grub-mkimage", "grub-mkstandalone", "grub-script-check"};
    std::vector<std::string> hints;
    for (auto s : allhints)
    {
      if (cmd_part == t.purge_ansi(s).substr(0, cmd_part.size()))
        hints.push_back(s);
    }
    //t.print_text(cmd_part);
    
    return hints;
  };
  t.reset_command("Some dummy command");
  
  t.run(exit);
  //t.run();
  //t.reset_command("Some command");
  return 0;
}

